# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [0.0.2] - 2020-05-17
### Fix
 - Fix the external folder to deploy the plugin after `maven install`.

## [0.0.1] - 2020-05-13
### Added
 - 
 - Add `CHANGELOG.md`, `README.md`
 - Add `distributionManagement` into `pom.xml`
